import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){


	const { user } = useContext(UserContext);

	return (

		<Navbar bg="light" expand="lg" sticky="top">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">DECODE PH</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            {
        	            	(user.isAdmin)
        	            	?
        	            	<Fragment>
        	            	<Nav.Link as={ NavLink } to="/products" end>Active Products</Nav.Link>
        	            	<Nav.Link as={ NavLink } to="/admin" end>Admin Dashboard</Nav.Link>
        	            	</Fragment>
        	            	:
        	           		<Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
        	            }
			            {(user.id !== null)
			            	?
			            	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			            	</Fragment>
			            }
			            
		          	</Nav>
		        </Navbar.Collapse>
		     </Container>
		</Navbar>
	)
}