import { Carousel } from 'react-bootstrap';

export default function Highlights() {
	return (
		  <Carousel>
		    <Carousel.Item>
		      <img
		        className="d-block w-100"
		        src="https://images.pexels.com/photos/8538756/pexels-photo-8538756.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
		        alt="First slide"
		        width={800} 
		        height={400}
		      />
		     {/* <Carousel.Caption>
		        <h3>TOBLERONE</h3>
		        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		      </Carousel.Caption>*/}
		    </Carousel.Item>
		    <Carousel.Item>
		      <img
		        className="d-block w-100"
		        src="https://images.pexels.com/photos/2072158/pexels-photo-2072158.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
		        alt="Second slide"
		        width={800} 
		        height={400}
		      />

		      {/*<Carousel.Caption>
		        <h3>Second slide label</h3>
		        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		      </Carousel.Caption>*/}
		    </Carousel.Item>
		    <Carousel.Item>
		      <img
		        className="d-block w-100"
		        src="https://images.pexels.com/photos/4791303/pexels-photo-4791303.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
		        alt="Third slide"
		        width={800} 
		        height={400}
		      />

		      {/*<Carousel.Caption>
		        <h3>Third slide label</h3>
		        <p>
		          Praesent commodo cursus magna, vel scelerisque nisl consectetur.
		        </p>
		      </Carousel.Caption>*/}
		    </Carousel.Item>
		  </Carousel>

	)

}