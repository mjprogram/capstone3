// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    console.log(productProp);

    // Deconstructs the product properties into their own variables
    const {_id, name, description, slots, price } = productProp;


    return (
        <Card className="m-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Available:</Card.Subtitle>
                <Card.Text>{slots}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}