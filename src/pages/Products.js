import { Fragment, useEffect, useState } from 'react';
import { Image } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import img1 from './images/products.jpg';

export default function Products() {
	
	// State that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			{/*<h1>Products</h1>*/}
			<Image src={img1} rounded fluid />
			{products}
		</Fragment>
	)
}