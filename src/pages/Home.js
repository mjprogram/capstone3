import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
	    title: "DECODE PH",
	    content: "You can’t buy happiness, but you can buy chocolates.",
	    destination: "/products",
	    label: "Buy Now!",
	}
	
	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}